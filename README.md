# RP (repositories)

List git repositories and give hints about their status.

See ```rp -h``` for help.

This script has two modes:

- list the configured repositories (faster). In this mode, scan all registered repositories (see the "configuration" chapter).
- list git repositories recursively found in a folder (~ by default) using -s/--scan. In this mode, the script may also save the repositories list in its config to use the second mode later (use -S/--save).

Use the -p/--pull option to pull all repositories.

## Install

- This script relies on highlightcli and configfile, install these repos in your PYTHONPATH first.
- There is no installer yet, you can use rp.py directly

## First use

run ```rp -s``` to scan all existing repos in the default search directory (~). Add the "-k" option to ignore already configured repositories.

run ```rp -S``` to scan all existing repos in ~ and store this list in the config file

run ```rp``` to list configured repos

run ```rp -f/--filter <field> <value>``` to filter the result, for example ```rp --filter remotes gitlab``` will discard repositories that have no remote on gitlab. The backend of this feature is a simple substring matching, nothing fancy yet. You can chain several ```--filter/-f``` arguments.

run ```rp -c/--command <cmd>``` to run a command for every listed repository. For each repository, CWD is set to the repository and the command is executed. You can use substitution strings on the following fields:

```
name
parent
path
branch
tag
dirty
untracked
unpushed
unpulled
remotes
```

eg.: ```rp -f branch master -c "git branch -m master main" ```

Note that cmd is run before listing the repositories but the listing is computed before and is not updated.

## Configuration

The config file is ```~/mrrabbit/rp/config.json``` according to the *configfile* workflow.

The -s/--scan option looks into the "search_dir" folder (default is ~).

All repositories absolute path (~ is expanded) are listed in "projects".

Example:

```json
{
    "projects": [
        "~/dev/important_repo"
    ],
    "extra_projects": [
        "~/dev/not_so_important"
    ],
    "search_dir": "~/dev"
}
```

Repositories in "projects" are always listed, repositories in "extra_projects" are listed if "-a/--all" is given.

## bash tools

### create repos on remote

```sh
mkdir -p <repos tree>
# git init in each leaf
for i in $(find . -type d -links 2)
    cd $i && git init . --bare  && cd ~/git
```

### add remotes on a cloned directory structure

If you have a lot of repos in ~ and the same structure on a git server, you can tell the local repos to add the corresponding path on the server. script.py:

```python
#!/usr/bin/env python

import sys
from subprocess import call
from shlex import split

path = sys.argv[1].replace("/home/user/", "")
call(split("git remote add main remote_server:{}".format(path)))
```

Then run ```rp -c $(readlink -f script.py)```

## TODO

- rp init() does everything and other comamnds (eg. fetch) are called later.
fetch should be called before computing the table.
- allow to list a group of repos in a file and apply batch commands on these repos
- filter also using status such as dirty
- allow zsh alias in commands
- create custom commands such as 'increment_patch', increment_minor', etc.
- get a ssh directory and list repositories not in local


