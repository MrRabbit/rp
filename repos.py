#!/usr/bin/env python

from git import (
    Repo,
    NoSuchPathError,
    InvalidGitRepositoryError,
    GitCommandError,
)
from pathlib import Path
from configfile import ConfigFile
from highlightcli import Table, highlight
from sys import stderr
from shlex import split
from subprocess import call
import os
from concurrent.futures import ThreadPoolExecutor

_debug = True


def log_error(fmt, *args, **kwargs):
    print(fmt, *args, file=stderr, **kwargs)


def log_debug(fmt, *args, **kwargs):
    if _debug:
        print(fmt, *args, **kwargs)


class RpError(Exception):
    pass


class RepoWrapper:
    idx = 1

    def __init__(self, r, parent=None):
        self.repo = r
        self.name = Path(r.working_tree_dir).name  # not the actual repo name
        try:  # try on the first git command
            self.dirty = r.is_dirty()
        except GitCommandError as e:
            log_error(f"git error on: {r}")
            log_error(e)
            raise RpError
        self.untracked = len(r.untracked_files) != 0
        self.tag = r.tags[-1].name if len(r.tags) else ""
        self.path = r.working_tree_dir
        self.parent = parent
        self.unpushed = 0
        self.unpulled = 0
        self.remote_urls = []
        self.from_sha = True
        self.name_color = "Light red"
        try:
            self.branch = r.active_branch.name
            self.from_sha = False
        except TypeError:
            self.branch = r.head.commit.hexsha[:8]
            pass
        ok = True
        try:
            head = r.head.ref
        except TypeError:
            log_debug(highlight(f"{self.name}: head is detached", "Light red"))
            ok = False
        if ok:
            # compare nuber of local and remote unshared deltas
            self.remote_urls = list(
                j.__next__()
                for j in (i for i in (rem.urls for rem in r.remotes))
            )
            tracking = head.tracking_branch()
            if tracking:
                try:
                    self.unpushed = len(
                        list(
                            head.commit.iter_items(
                                r, f"{tracking.path}..{head.path}"
                            )
                        )
                    )
                    self.unpulled = len(
                        list(
                            tracking.commit.iter_items(
                                r, f"{head.path}..{tracking.path}"
                            )
                        )
                    )
                    self.name_color = "Green"
                except OSError:
                    log_debug(
                        highlight(
                            f"{self.name}: OS error (may be too big)",
                            "Light red",
                        )
                    )
                except GitCommandError:
                    log_debug(
                        highlight(
                            f"{self.name}: git error",
                            "Light red",
                        )
                    )
            else:
                log_debug(
                    highlight("{self.name}: has no tracking", "Light red")
                )
        # compute string representation
        if self.parent:
            self.prefix_str = "└"
            self.prefix_color = None
        else:
            self.prefix_str = RepoWrapper.idx
            RepoWrapper.idx += 1
            self.prefix_color = "Cyan"
        self.untracked_str = "!" if self.untracked else ""
        self.delta_str = (
            "↑%2d ↓%2d" % (self.unpushed, self.unpulled)
            if self.unpushed or self.unpulled
            else ""
        )
        self.branch_str = "" if self.branch == "main" else self.branch
        self.set_colors()

    def set_colors(self):
        self.path_color = "Light gray"
        if self.dirty:
            self.name_color = "Magenta"
        elif self.unpushed != 0:
            self.name_color = "Cyan"
        elif self.unpulled != 0:
            self.name_color = "Light cyan"
        else:  # everything is up to date
            self.path_color = "Dark gray"

        if self.from_sha:
            self.branch_color = "Light red"
        else:
            self.branch_color = "Light magenta"

    def matches(self, cat, value):
        # textual params
        if cat in ["name", "path", "branch", "tag", "remotes"]:
            if value in getattr(self, cat, ""):
                return True
        # boolean params
        if cat in ["dirty", "unpushed", "unpulled"]:
            req = value.lower() in ["1", "true", "yes", "y"]
            actual = getattr(self, cat, False)
            if req == actual:
                return True
        return False

    def substitue(self, text):
        fields = [
            "name",
            "parent",
            "path",
            "branch",
            "tag",
            "dirty",
            "untracked",
            "unpushed",
            "unpulled",
            "remotes",
        ]
        for i in fields:
            text = text.replace("{%s}" % i, str(getattr(self, i, "")))
        return text


class RepoManager:
    def __init__(self, jobs, *, debug=False):
        global _debug
        _debug = debug
        self.cf = ConfigFile("mrrabbit", "rp")
        self.paths = []
        self.repos = []
        self.jobs = jobs
        self.changed = False

    def getDirectoryList(self, path):
        return [
            x.parent.as_posix() for x in sorted(Path(path).glob("**/.git"))
        ]

    def retrieve_paths(self, scan, save, include_extra, ignore_known):
        # list configured known repos
        known_main = [
            os.path.expanduser(i) for i in self.cf.get("projects", default=[])
        ]
        known_all = known_main + [
            os.path.expanduser(i)
            for i in self.cf.get("extra_projects", default=[])
        ]
        exclude = [
            os.path.expanduser(i)
            for i in self.cf.get("exclude_dir", default=[])
        ]
        ignore = self.cf.get("ignore", default=[])
        projects_dir = os.path.expanduser(
            self.cf.get("search_dir", default="~")
        )
        if not known_main:
            log_error("config file seems invalid")
        # scan all repos in search dir
        if scan or save:
            if ignore_known:
                repos = [
                    repo
                    for repo in self.getDirectoryList(projects_dir)
                    if not any([repo.startswith(i) for i in known_all])
                ]
            else:
                repos = self.getDirectoryList(projects_dir)
            if save:
                self.cf.setField("projects", repos, immediate=True)
        # get known repos
        else:
            if include_extra:
                repos = known_all
            else:
                repos = known_main
        # exclude repos
        for r in repos:
            for ign in ignore:
                if ign in r:
                    break
            else:
                for excl in exclude:
                    if r.startswith(excl):
                        break
                else:
                    self.paths.append(r)

    def parse_all(self):
        with ThreadPoolExecutor(max_workers=self.jobs) as thr:
            for i in self.paths:
                thr.submit(self.parse_repo(i))

    def parse_repo(self, path, parent=None):
        log_debug(highlight(f"parsing: {path}", "Dark gray"))
        try:
            r = Repo(path)
        except NoSuchPathError:
            log_error(f"no such path: {path}")
            return
        except InvalidGitRepositoryError:
            log_error(f"not a git repository: {path}")
            return
        try:
            self.repos.append(RepoWrapper(r, parent))
        except RpError:
            pass
        try:
            for s in sorted(
                r.submodules, key=lambda x: x.module().working_tree_dir
            ):
                sub_path = s.module().working_tree_dir
                self.parse_repo(sub_path, r)
        except InvalidGitRepositoryError:
            log_error(f"submodule error at: {path}")
            pass
        r.close()

    def print_repos(self):
        if self.changed:
            self.repos = []
            self.parse_all()

        t = Table(align_right=[0], hide_alternator=[0], alternate=True)
        for row_idx, repo in enumerate(self.repos):
            # format remotes
            remotes = highlight("no remote", "Light red")
            if len(repo.remote_urls):
                remotes = highlight(repo.remote_urls[0], "Dark gray")
                for i in range(1, len(repo.remote_urls)):
                    remotes += highlight(" | ", "Blue")
                    remotes += highlight(repo.remote_urls[i], "Dark gray")
            # add all to table
            t.add_field(repo.prefix_str or row_idx, repo.prefix_color)
            t.add_field(repo.name, repo.name_color, max_width=40)
            t.add_field(repo.branch_str, repo.branch_color, max_width=20)
            t.add_field(repo.untracked_str, "Red")
            t.add_field(repo.delta_str, "Light red")
            t.add_field(repo.branch_str, repo.branch_color, max_width=20)
            t.add_field(repo.tag, "Light yellow", max_width=20)
            t.add_field(repo.path, repo.path_color, max_width=100)
            t.add_field(remotes, max_width=100)
            t.new_row()
        t.show()

    def fetch_all(self):
        with ThreadPoolExecutor(max_workers=self.jobs) as fetcher:
            for i in self.repos:
                for r in i.repo.remotes:
                    fetcher.submit(r.fetch)
        self.changed = True

    def filter_repos(self, filters):
        filtered_paths = []
        filtered_repos = []
        RepoWrapper.idx = 1
        for r in self.repos:
            for cat, value in filters:
                if r.matches(cat, value):
                    if r.parent is None:  # avoid dubplicate submodules
                        print(r.path)
                        filtered_repos.append(r)
                        filtered_paths.append(r.path)
        self.paths = filtered_paths
        self.repos = filtered_repos

    def init_config(self):
        data = {
            "search_dir": "~",
            "projects": [],
            "extra_projects": [],
            "exclude_dir": [],
        }
        self.cf.setRoot(data, True)

    def run_cmd(self, command):
        for r in self.repos:
            log_debug(f"running in {r.path}")
            cmd = r.substitue(command)
            ret = call(split(cmd), cwd=r.path)
            if ret != 0:
                log_error(f"{cmd} returned {ret}")
        self.changed = True
