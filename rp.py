#!/usr/bin/env python

import argparse
from repos import RepoManager


__version__ = "0.0.3"

parser = argparse.ArgumentParser(
    description="get the status of git repositories"
)
parser.add_argument(
    "-v",
    "--version",
    action="version",
    version="%(prog)s {version}".format(version=__version__),
)
parser.add_argument(
    "-e",
    "--fetch",
    help="fetch repositories, longer but parallelized",
    action="store_true",
)
parser.add_argument(
    "-a",
    "--all",
    help="list secondary repositories as well",
    action="store_true",
)
parser.add_argument(
    "-s",
    "--scan",
    help="scan repositories instead of using the list file",
    action="store_true",
)
parser.add_argument(
    "-S",
    "--save",
    help="scan repositories and save the list in the config file",
    action="store_true",
)
parser.add_argument(
    "-k",
    "--ignore-known",
    help="ignore known repositories",
    action="store_true",
)

parser.add_argument(
    "-j",
    "--jobs",
    help="number of threads to parallelize some tasks",
    default=16,
    type=int,
)

parser.add_argument(
    "-i",
    "--init",
    help="create an empty config file",
    action="store_true",
)

parser.add_argument(
    "-f",
    "--filter",
    nargs="+",
    help="filter the repos on path, branch or remote",
    action="append",
)

parser.add_argument(
    "-c",
    "--command",
    help="run a command on each repo",
)

parser.add_argument(
    "-d", "--debug", help="add debug output", action="store_true"
)
args = vars(parser.parse_args())

repos = RepoManager(jobs=args["jobs"], debug=args["debug"])

if args["init"]:
    repos.init_config()

repos.retrieve_paths(
    args["scan"], args["save"], args["all"], args["ignore_known"]
)
repos.parse_all()


if args["filter"]:
    repos.filter_repos(args["filter"])

if args["command"]:
    repos.run_cmd(args["command"])

if args["fetch"]:
    repos.fetch_all()

repos.print_repos()
